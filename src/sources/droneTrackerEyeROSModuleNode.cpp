#include "ros/ros.h"
#include "droneTrackerEyeROSModule.h"
#include "nodes_definition.h"


int main (int argc,char **argv)
{
    //ROS init
    ros::init(argc, argv, MODULE_NAME_TRACKER_EYE);
    ros::NodeHandle n;
    std::cout << "[ROSNODE] Starting" << ros::this_node::getName() << std::endl;

    DroneTrackerEyeROSModule MyTrackerEye;
    MyTrackerEye.open(n);


    try
    {
        // Loop
        ros::spin();
    }
    catch (std::exception &ex)
    {
        std::cout<<"[ROSNODE] Exception :"<<ex.what()<<std::endl;
    }

    return 0;
}
