#ifndef DRONETRACKEREYEROSMODULE_H
#define DRONETRACKEREYEROSMODULE_H

#include <string>
#include <ostream>
#include <thread>

////// ROS  ///////
#include "ros/ros.h"

// module dependencies
#include "droneModuleROS.h"

#include "droneMsgsROS/BoundingBox.h"
#include "std_msgs/Float32.h"
#include "std_msgs/Bool.h"

// rostopic hz /OpenTLD/tld_fps
#define DRONETRACKEREYEROSMODULE_TLD_EXPECTED_FPSCHANNEL_RATE ( 15.0 )
//#define DRONETRACKEREYEROSMODULE_TEST_CALLBACK_GET_DATA_CORRUPTION

class DroneTrackerEyeROSModule : public DroneModule
{
protected:
    std::thread* fps_thread;
    void fps_thread_main();

private:
    ros::NodeHandle n;
    // OpenTLD related resources
    ros::Subscriber bounding_box_sub;
    ros::Subscriber fps_sub;

    ros::Publisher tracking_object_pub;
    ros::Publisher is_object_on_frame_pub;
    ros::Publisher get_bounding_box_pub;

    droneMsgsROS::BoundingBox bounding_box;
    std_msgs::Float32 fps;
    std_msgs::Bool tracking_object_tbp;
    std_msgs::Bool is_object_on_frame_tbp;
    droneMsgsROS::BoundingBox get_bounding_box_tbp;


    void boundingBoxSubCallback(const droneMsgsROS::BoundingBox::ConstPtr &msg);
    void fpsSubCallback(const std_msgs::Float32::ConstPtr &msg);

protected:

   // bool reset();

public:
     void init();
     void close();

protected:
     bool resetValues();
     bool startVal();
     bool stopVal();

public:

     bool run();

public:
     void open(ros::NodeHandle & nIn);

public: //topics
     std::string Open_tld_translator_tracked_object_topic_name;
     std::string Open_tld_translator_fps_topic_name;
     std::string tracking_object_topic_name;
     std::string is_object_on_frame_topic_name;
     std::string get_bounding_box_topic_name;


private: // OpenTLD node status
    bool tracking_object;
    bool is_object_on_frame;
    bool received_fps_atleast_once;
    ros::Time last_time_recieved_fps;
    ros::Time last_time_recieved_bb;

public:
    DroneTrackerEyeROSModule( );
    ~DroneTrackerEyeROSModule();

//private: bool moduleOpened;
public:
    //void open(ros::NodeHandle & nIn, std::string ardroneName);
    bool isTrackingObject();
    bool isObjectOnFrame();
    void print();
    bool getBoundingBox();
    void publishTrackingObject();
    void publishIsObjectOnFrame();
    void publishGetBoundingBox();

};

#endif // DRONETRACKEREYEROSMODULE_H
